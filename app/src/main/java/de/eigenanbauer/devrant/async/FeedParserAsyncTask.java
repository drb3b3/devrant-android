package de.eigenanbauer.devrant.async;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import de.eigenanbauer.devrant.data.FeedType;
import de.eigenanbauer.devrant.data.LoginData;
import de.eigenanbauer.devrant.data.Rant;
import de.eigenanbauer.devrant.data.Tag;
import de.eigenanbauer.devrant.data.VoteState;

/**
 * An AsyncTask that loads all rants of a page and passes them to the feed parser.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class FeedParserAsyncTask extends AsyncTask<String, Void, String> {

    private static final String TAG = "FEED_PARSER_ASYNC_TASK";

    private FeedParser feedParser;

    public FeedParserAsyncTask(FeedParser feedParser) {
        this.feedParser = feedParser;
    }

    /**
     * Parses all rants of the current page, adds them to the rant collector and navigates to the next page.
     * @param strings Not used.
     * @return Nothing. Null.
     */
    @Override
    protected String doInBackground(String... strings) {
        ArrayList<Rant> rants = new ArrayList<>();

        String url = getPageURL();
        Log.d(TAG, "Loading new rants from " + url + ".");

        try {
            Document doc = Jsoup.connect(url).cookies(LoginData.getCookies()).get();
            Elements rantsElems = doc.getElementsByClass("rant-comment-row-widget");

            for (Element rant : rantsElems) {
                String rantUrl = rant.getElementsByClass("rantlist-title").get(0).absUrl("href");
                String text = rant.getElementsByClass("rantlist-title-text").get(0).html().replace("<br> ", "");
                String imgURL = null;
                ArrayList<Tag> tags = new ArrayList<>();
                int commentCount = 0, voteCount = 0;
                VoteState voteState;
                int dataId;

                /* get the img url if the rant has a image. */
                Elements imgs = rant.getElementsByTag("img");
                if (imgs.size() == 1) {
                    imgURL = imgs.get(0).absUrl("src");
                }

                Elements tagsElems = rant.getElementsByClass("rantlist-tags").get(0).getElementsByTag("a");
                for (Element tagElem : tagsElems) {
                    tags.add(new Tag(tagElem.html(), tagElem.absUrl("href")));
                }

                /* parse the amount of comments */
                Elements commentCountElems = rant.getElementsByClass("commment-num");
                if (commentCountElems.size() != 0)
                    commentCount = Integer.valueOf(commentCountElems.get(0).html());


                /* parse votes */
                Element voteWidgetElem = rant.getAllElements().get(1);
                Elements voteCountElems = rant.getElementsByClass("votecount");
                if (voteCountElems.size() != 0) {
                    voteCount = Integer.valueOf(voteCountElems.get(0).html());
                }

                if (voteWidgetElem.attr("class").endsWith("upvoted")) {
                    voteState = VoteState.UPVOTED;
                } else if (voteWidgetElem.attr("class").endsWith("downvoted")) {
                    voteState = VoteState.DOWNVOTED;
                } else {
                    voteState = VoteState.UNVOTED;
                }

                dataId = Integer.parseInt(rant.attr("data-id"));

                rants.add(new Rant(rantUrl, text, imgURL, tags, commentCount, voteCount, voteState, dataId));
            }
        } catch (IOException e) {
            e.printStackTrace();
            feedParser.onNetworkError();
            Log.e(TAG, "Error doing JSoup request.");
            return null;
        }

        if (feedParser.isEnabled())
            feedParser.onLoadingFinished(rants);
        else
            Log.w(TAG, "Not adding rants to adapter because it disabled us.");


        return null;
    }


    /**
     * @return A URL pointing to the feed page based on the current feed type and page.
     */
    private String getPageURL() {
        String feedString = getFeedStringFromType(feedParser.getFeedType());
        return "https://devrant.com/feed/" + feedString + "/" + feedParser.getCurrentPage();
    }

    /**
     * @param type The feed type that should be represented as a string.
     * @return A string representation of a feed type.
     */
    private static String getFeedStringFromType(FeedType type) {
        switch (type) {
            case FEEDTYPE_ALGO:
                return "algo";
            case FEEDTYPE_RECENT:
                return "recent";
            case FEEDTYPE_TOP_ALL:
                return "top";
            case FEEDTYPE_TOP_MONTH:
                return "top/month";
            case FEEDTYPE_TOP_WEEK:
                return "top/week";
            case FEEDTYPE_TOP_DAY:
                return "top/day";
        }

        return "recent";
    }
}
