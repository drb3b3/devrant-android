package de.eigenanbauer.devrant.async;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

import de.eigenanbauer.devrant.RantVoteSuccessListener;
import de.eigenanbauer.devrant.adapters.FeedAdapter;
import de.eigenanbauer.devrant.data.LoginData;
import de.eigenanbauer.devrant.data.VoteState;

/**
 * An AsyncTask to access the devRant API endpoint for voting a given rant.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class FeedRantVoter extends AsyncTask<String, Void, String> {

    private static final String TAG = "FEED_RANT_VOTER";

    private RantVoteSuccessListener successListener;
    private VoteState voteState;
    private int rantId;

    /**
     * Creates a new FeedRantVoter
     * @param successListener A listener to tell about voting success and failure.
     * @param voteState Whether to upvote, unvote or downvote the rant.
     * @param rantId The ID of the rant to vote.
     */
    public FeedRantVoter(RantVoteSuccessListener successListener, VoteState voteState, int rantId) {
        this.successListener = successListener;
        this.voteState = voteState;
        this.rantId = rantId;
    }

    /**
     * Accesses the devRant API endpoint, passes the users credentials in the request.
     * Tells the success listener that there was a failure in case the vote state was unknown,
     * there was an error while parsing the JSON data returned by the devRant API, there was a
     * network error or any other error.
     * Tells the success listener when voting was successfull and passes the new vote state and the new vote count.
     * @param strings Not used.
     * @return Nothing
     */
    @Override
    protected String doInBackground(String... strings) {
        try {
            int vote;
            if (voteState == VoteState.UNVOTED)
                vote = 0;
            else if (voteState == VoteState.UPVOTED)
                vote = 1;
            else if (voteState == VoteState.DOWNVOTED)
                vote = -1;
            else {
                successListener.onVoteFailure("Error: Unknown vote state.");
                return null;
            }

            String url = "https://devrant.com/api/devrant/rants/" + rantId + "/vote";
            Log.d(TAG, "Nice url: " + url);
            Connection con = Jsoup.connect(url)
                    .method(Connection.Method.POST)
                    .ignoreContentType(true)
                    .ignoreHttpErrors(true)
                    .data("app", "3")
                    .data("token_id", new Integer(LoginData.getAuthToken().getTokenId()).toString())
                    .data("user_id", new Integer(LoginData.getAuthToken().getUserId()).toString())
                    .data("token_key", LoginData.getAuthToken().getTokenKey())
                    .data("guid", LoginData.getGuid())
                    .data("plat", "3")
                    .data("vote", new Integer(vote).toString())
                    .data("reason", "0")
                    .cookies(LoginData.getCookies());
            Connection.Response res = con.execute();
            Log.d(TAG, "token_id: " + LoginData.getAuthToken().getUserId());
            Log.d(TAG, res.body());
            if (res.statusCode() == 200) {
                JSONObject jobj = new JSONObject(res.body());
                int newVoteCount = jobj.getJSONObject("rant").getInt("score");
                if (voteState == VoteState.UPVOTED)
                    successListener.onVoteSuccess("Upvote successful", voteState, newVoteCount);
                else if (voteState == VoteState.UNVOTED)
                    successListener.onVoteSuccess("Unvote successful.", voteState, newVoteCount);
                else if (voteState == VoteState.DOWNVOTED)
                    successListener.onVoteSuccess("Downvote successful.", voteState, newVoteCount);
                else
                    successListener.onVoteSuccess("Holy shit what just happened????!", voteState, newVoteCount);
            } else {
                successListener.onVoteFailure("Voting was unsuccessful.");
            }
        } catch (IOException e) {
            e.printStackTrace();
            successListener.onVoteFailure("An IO error occured.");
        } catch (JSONException e) {
            e.printStackTrace();
            successListener.onVoteFailure("Error parsing JSON.");
        }

        return null;
    }
}
