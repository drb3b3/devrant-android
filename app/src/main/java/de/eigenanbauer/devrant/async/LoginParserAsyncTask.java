package de.eigenanbauer.devrant.async;

import android.os.AsyncTask;
import android.util.JsonWriter;
import android.util.Log;

import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

import de.eigenanbauer.devrant.activities.LoginActivity;
import de.eigenanbauer.devrant.data.LoginData;

/**
 * A parser that is able to log the user into devRant by accessing its API.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class LoginParserAsyncTask extends AsyncTask<String, Void, String> {

    private static final String TAG = "LOGIN_PARSER";

    private LoginActivity loginActivity;
    private String username;
    private String password;

    /**
     * @param activity The login activity to tell about failure.
     * @param username The username to use for logging in.
     * @param password The password to use for logging in.
     */
    public void login(LoginActivity activity, String username, String password) {
        Log.d(TAG, "login()");
        this.loginActivity = activity;
        this.username = username;
        this.password = password;
        execute();
    }

    /**
     * Accesses the devRant API with the current set login values.
     * On failure or success the login activity is told about it.
     * @param strings Not used.
     * @return Nothing.
     */
    @Override
    protected String doInBackground(String... strings) {
        String json =
                "{" +
                        "\"app\": 3," +
                        "\"username\": \"" + username + "\"," +
                        "\"password\": \"" + password + "\"," +
                        "\"guid\": \"" + LoginData.getGuid() + "\"," +
                        "\"plat\": 3" +
                        "}";

        Log.d(TAG, json);

        try {
            Connection.Response res = Jsoup.connect("https://devrant.com/api/users/auth-token")
                    .ignoreContentType(true) /* so jsoup doesn't die when json is returned */
                    .ignoreHttpErrors(true) /* so jsoup doesn't die on http errors */
                    .method(Connection.Method.POST)
                    .header("Content-Type", "application/json")
                    .requestBody(json)
                    .execute();
            if (res.statusCode() != 200) {
                Log.e(TAG, "Logging in failed." + res.body());
                loginActivity.onFailedLogin(res.body());
                return null;
            } else {
                Log.d(TAG, res.body());
                loginActivity.onSuccessfulLogin(username, res.body());
                return res.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "Something fucked up the login request.");
            loginActivity.onFailedLogin("");
            return null;
        }
    }
}
