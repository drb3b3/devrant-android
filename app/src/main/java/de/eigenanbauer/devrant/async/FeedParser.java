package de.eigenanbauer.devrant.async;

import android.util.Log;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import de.eigenanbauer.devrant.RantCollector;
import de.eigenanbauer.devrant.data.FeedType;
import de.eigenanbauer.devrant.data.Rant;

/**
 * A parser that is able to parse new pages from the devRant feed based on a given feed type.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class FeedParser {

    private static final String TAG = "FEED_PARSER";

    private AtomicBoolean loading = new AtomicBoolean(false);
    private AtomicBoolean enabled = new AtomicBoolean(true);
    private RantCollector collector;
    private FeedType feedType;

    int page = 1;

    /**
     * Creates a new FeedParser. Defaults the type of the feed to "recent".
     * @param collector The collector to add freshly loaded rants to and tell about errors.
     */
    public FeedParser(RantCollector collector) {
        this.collector = collector;
        this.feedType = FeedType.FEEDTYPE_RECENT;
    }

    /**
     * Creates a new FeedParser.
     * @param collector The collector to add freshly loaded rants to and tell about errors.
     * @param type The feed type to load rants from.
     */
    public FeedParser(RantCollector collector, FeedType type) {
        this.collector = collector;
        this.feedType = type;
    }

    /**
     * Starts loading new rants if it is not already loading at the moment.
     */
    public void loadIfNotAlreadyLoading() {
        if (loading.compareAndSet(false, true)) {
            new FeedParserAsyncTask(this).execute();
        }
    }

    /**
     * @return The next page the parser would parse.
     */
    public int getCurrentPage() {
        return page;
    }

    /**
     * Adds rants to the collector passed in the constructor. Also navigates to the next page of the feed.
     * @param rants
     */
    public void onLoadingFinished(ArrayList<Rant> rants) {
        collector.addRants(rants);
        page += 1;
        loading.set(false);
    }

    /**
     * @return Whether the parser is enabled (default) or has been disabled by it's adapter.
     */
    public boolean isEnabled() {
        return enabled.get();
    }

    /**
     * Get's called when a network error occurs while loading a new feed page.
     * Tells the rant collector about the error.
     */
    public void onNetworkError() {
        collector.onNetworkError();
    }

    /**
     * @return The current feed type.
     */
    public FeedType getFeedType() {
        return feedType;
    }

    /**
     * Disables the feed parser so that all rants that are loaded from now on wont be added to the collector.
     */
    public void disable() {
        enabled.set(false);
        Log.d(TAG, "disable()");
    }
}
