package de.eigenanbauer.devrant.async;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

import de.eigenanbauer.devrant.activities.LoginActivity;
import de.eigenanbauer.devrant.data.LoginData;

/**
 * A parser for accessing devRants logic API call. If the login is successful a message is
 * broadcasted to everyone registered in the LoginData.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class LoginParser {

    private static final String TAG = "LOGIN_PARSER";

    public static void login(LoginActivity activity , String username, String password) {
        Log.d(TAG, "login()");
        LoginTask loginTask = new LoginTask(activity);
        loginTask.execute(username, password);
    }

    private static class LoginTask extends AsyncTask<String, Void, String> {

        private static final String TAG = "LOGIN_TASK";

        private LoginActivity activity;

        public LoginTask(LoginActivity activity) {
            super();

            this.activity = activity;
        }

        @Override
        protected String doInBackground(String... strings) {
            if (strings.length != 2) {
                Log.e(TAG, "Didn't pass username and password to LoginTask.");
                return null;
            }
            String username = strings[0];
            String password = strings[1];
            String json =
                    "{" +
                            "\"app\": 3," +
                            "\"username\": \"" + username + "\"," +
                            "\"password\": \"" + password+ "\"," +
                            "\"guid\": \"" + LoginData.getGuid() + "\"," +
                            "\"plat\": 3" +
                            "}";

            Log.d(TAG, json);

            try {
                Connection.Response res = Jsoup.connect("https://devrant.com/api/users/auth-token")
                        .ignoreContentType(true) /* so jsoup doesn't die when json is returned */
                        .ignoreHttpErrors(true) /* so jsoup doesn't die on http errors */
                        .method(Connection.Method.POST)
                        .header("Content-Type", "application/json")
                        .requestBody(json)
                        .execute();
                if (res.statusCode() != 200) {
                    Log.e(TAG, "Logging in failed." + res.body());
                    activity.onFailedLogin(res.body());
                    return null;
                } else {
                    Log.d(TAG, res.body());
                    activity.onSuccessfulLogin(username, res.body());
                    return res.body();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Something fucked up the login request.");
                activity.onFailedLogin("");
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}
