package de.eigenanbauer.devrant.async;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

import de.eigenanbauer.devrant.RantPostSuccessListener;
import de.eigenanbauer.devrant.data.LoginData;

/**
 * An AsyncTask to post rants by accessing the devRant API.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class RantPosterAsyncTask extends AsyncTask<String, Void, String> {

    private RantPostSuccessListener successListener;
    private String rant;
    private String tags;

    /**
     * Creates a new RantPosterAsyncTask
     * @param successListener The listener to tell about success or failure.
     * @param rant The text of the rant.
     * @param tags The tags of the rant.
     */
    public RantPosterAsyncTask(RantPostSuccessListener successListener, String rant, String tags) {
        this.successListener = successListener;
        this.rant = rant;
        this.tags = tags;

        execute();
    }

    /**
     * Accesses the devRant API and passes all required data to it. If there was a failure when posting
     * If the devRant API returs an error or there was a general error the success listener is told about it.
     * On success the success listener is notified and given the json answer.
     * @param strings Not used.
     * @return Nothing.
     */
    @Override
    protected String doInBackground(String... strings) {
        try {
            String url = "https://devrant.com/api/devrant/rants";
            Connection con = Jsoup.connect(url)
                    .method(Connection.Method.POST)
                    .ignoreContentType(true)
                    .ignoreHttpErrors(true)
                    .data("app", "3")
                    .data("rant", rant)
                    .data("tags", tags)
                    .data("token_id", new Integer(LoginData.getAuthToken().getTokenId()).toString())
                    .data("token_key", LoginData.getAuthToken().getTokenKey())
                    .data("user_id", new Integer(LoginData.getAuthToken().getUserId()).toString())
                    .data("type", "1")
                    .cookies(LoginData.getCookies());
            Connection.Response res = con.execute();

            JSONObject jsonObject = new JSONObject(res.body());
            if (!jsonObject.getBoolean("success")) {
                successListener.onRantPostFailure(res.body());
                return null;
            }

            successListener.onRantPostSuccess(res.body());
        } catch (IOException e) {
            e.printStackTrace();
            successListener.onRantPostFailure("IOException");
        } catch (JSONException e) {
            e.printStackTrace();
            successListener.onRantPostFailure("JSONException");
        }

        return null;
    }
}
