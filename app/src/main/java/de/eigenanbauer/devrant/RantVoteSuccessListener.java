package de.eigenanbauer.devrant;

import de.eigenanbauer.devrant.data.VoteState;


/**
 * An interface that handles success and failure when voting a rant.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public interface RantVoteSuccessListener {

    /**
     * Called when the vote was successful.
     * @param msg The success message.
     * @param newVoteState The new vote state.
     * @param newVoteCount The new vote count after voting.
     */
    void onVoteSuccess(final String msg, final VoteState newVoteState, final int newVoteCount);

    /**
     * Called when the vote failed.
     * @param msg The message return by the devRant API.
     */
    void onVoteFailure(final String msg);

}
