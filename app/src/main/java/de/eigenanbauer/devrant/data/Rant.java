package de.eigenanbauer.devrant.data;

import java.util.ArrayList;

/**
 * Representation of a rant as displayed on the feed.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class Rant {

    private String rantURL;
    private String text;
    private String imageURL;
    private ArrayList<Tag> tags;
    private int commentCount;
    private int voteCount;
    private VoteState voteState;
    private int dataId;

    /**
     * @return The URL the rant is available at.
     */
    public String getRantURL() {
        return rantURL;
    }

    /**
     * @param rantURL The rants new URL.
     */
    public void setRantURL(String rantURL) {
        this.rantURL = rantURL;
    }

    /**
     * @return The rants text.
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The rants new text.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return A URL to the image of the rant.
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * @param imageURL The new URL pointing to the image of the rant.
     */
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    /**
     * @return All tags the rant was tagged with.
     */
    public ArrayList<Tag> getTags() {
        return tags;
    }

    /**
     * @param tags All new tags of the rant.
     */
    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    /**
     * @return The number of times the rant was commented.
     */
    public int getCommentCount() {
        return commentCount;
    }

    /**
     * @param commentCount The new amount of comments.
     */
    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    /**
     * @return The vote count (likes increment this value whereas each dislike decrements it).
     */
    public int getVoteCount() {
        return voteCount;
    }

    /**
     * @param voteCount The new amount of votes.
     */
    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    /**
     * @return The current vote state of the rant. May be one of UPVOTED, DOWNVOTED or UNVOTED.
     */
    public VoteState getVoteState() {
        return voteState;
    }

    /**
     * @param voteState The new vote state.
     */
    public void setVoteState(VoteState voteState) {
        this.voteState = voteState;
    }

    /**
     * @return The rants data ID which is required identifying the rant in various devRant API calls.
     */
    public int getDataId() {
        return dataId;
    }

    /**
     * @param dataId The rants new data ID.
     */
    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    /**
     * Generates a new rant.
     * @param rantURL
     * @param text
     * @param imageURL
     * @param tags
     * @param commentCount
     * @param voteCount
     * @param voteState
     * @param dataId
     */
    public Rant(String rantURL, String text, String imageURL, ArrayList<Tag> tags, int commentCount, int voteCount, VoteState voteState, int dataId) {
        this.rantURL = rantURL;
        this.text = text;
        this.imageURL = imageURL;
        this.tags = tags;
        this.commentCount = commentCount;
        this.voteCount = voteCount;
        this.voteState = voteState;
        this.dataId = dataId;
    }

    /**
     * @return A tag string containing all of the rants tag comma separated.
     */
    public String createTagsString() {
        String tagsString = "";

        for (Tag tag : tags) {
            tagsString += tag.getName();
            if (tags.indexOf(tag) != tags.size() - 1)
                tagsString += ", ";
        }

        return tagsString;
    }
}
