package de.eigenanbauer.devrant.data;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import de.eigenanbauer.devrant.R;

/**
 * Simple global settings interface to easily query various settings.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class Settings {

    public static boolean getUseDarkTheme(Application app) {
        SharedPreferences prefs = app.getSharedPreferences("devrant.eigenanbauer.de.devrant_preferences", Context.MODE_PRIVATE);
        return prefs.getBoolean(app.getString(R.string.preference_key_darktheme_switch), false);
    }
}
