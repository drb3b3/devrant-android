package de.eigenanbauer.devrant.data;

/**
 * Tells whether the user didn't vote, upvoted or downvoted a given rant or comment.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public enum VoteState {
    /**
     * The user didn't vote the rant of comment.
     */
    UNVOTED,

    /**
     * The user liked the rant or comment.
     */
    UPVOTED,

    /**
     * The user disliked the rant or comment.
     */
    DOWNVOTED,
}
