package de.eigenanbauer.devrant.data;

/**
 * Representation of a tag. A tag is a combination of the tags name (how it is displayed) and a URL to query all rants by a given tag.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class Tag {

    private String name;
    private String searchURL;

    public Tag(String name, String searchURL) {
        this.name = name;
        this.searchURL = searchURL;
    }

    /**
     * @return The tags name as displayed.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The tags new name to be displayed.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The tags URL.
     */
    public String getSearchURL() {
        return searchURL;
    }

    /**
     * @param searchURL The tags new URL.
     */
    public void setSearchURL(String searchURL) {
        this.searchURL = searchURL;
    }
}
