package de.eigenanbauer.devrant.data;

/**
 * Different feed types that can be viewed from in the feed fragment.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public enum FeedType {
    /**
     * Let devRant decide what to show.
     */
    FEEDTYPE_ALGO,

    /**
     * Show the most recent rants.
     */
    FEEDTYPE_RECENT,

    /**
     * The the best rants of all time.
     */
    FEEDTYPE_TOP_ALL,

    /**
     * The best rants of the current month.
     */
    FEEDTYPE_TOP_MONTH,

    /**
     * The best rants of the current week.
     */
    FEEDTYPE_TOP_WEEK,

    /**
     * The best rants of today.
     */
    FEEDTYPE_TOP_DAY,
}
