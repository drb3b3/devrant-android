package de.eigenanbauer.devrant.data;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import de.eigenanbauer.devrant.LoginStateListener;
import de.eigenanbauer.devrant.R;

/**
 * A global set of structures and functions to access various data required for the interaction with the devRant API.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class LoginData {

    private static final String TAG = "LOGIN_DATA";

    private static ArrayList<LoginStateListener> loginStateListeners = new ArrayList<>();

    private static  AuthToken authToken;
    private static String guid;
    private static String username;

    /**
     * @return True if the user is logged in, otherwise false.
     */
    public static boolean hasUserCredentials() {
        return (authToken != null);
    }

    /**
     * @return A string representation of the users authentication token.
     */
    public static String getAuthTokenAsString() {
        return authToken.toString();
    }

    /**
     * Sets the global username.
     * @param newUsername The users username.
     */
    public static void setUsername(String newUsername) {
        username = newUsername;
        broadcastLoginStateChanged();
    }

    /**
     * @return The global username.
     */
    public static String getUsername() {
        return username;
    }

    /**
     * @return The authentication token as an actual easy to use Java Object.
     */
    public static AuthToken getAuthToken() { return authToken; }

    /**
     * @param json The users credentials to set.
     */
    public static void setCredentials(String json) {
        authToken = new AuthToken(json);
        broadcastLoginStateChanged();
    }

    /**
     * @return The users GUID.
     */
    public static String getGuid() {
        return guid;
    }

    /**
     * Generates a HashMap that contains all cookies required to access all parts of the devRant API.
     * @return The cookies HashMap.
     */
    public static Map<String, String> getCookies() {
        Map<String, String> cookies = new HashMap<>();

        if (guid != null)
            cookies.put("dr_guid", guid);
        if (authToken != null) {
            cookies.put("dr_token", authToken.toString());
        }

        return cookies;
    }

    /**
     * Saves the users credentials to the shared preferences. Only saves values
     * that are currently set. If the user is not logged in, no authentication token will be saved.
     * @param app The application whose shared prefs to use.
     */
    public static void saveCredentials(Application app) {
        SharedPreferences prefs = app.getSharedPreferences("devRant", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        if (guid != null) editor.putString(app.getString(R.string.preference_guid), guid);
        if (authToken != null) editor.putString(app.getString(R.string.preference_token), authToken.toString());
        if (username != null) editor.putString(app.getString(R.string.preference_username), username);

        editor.commit();
    }

    /**
     * Loads the users credentials. If a value is not set the default is null.
     * If the guid is null, a random new guid is generated.
     * @param app
     */
    public static void loadSavedCredentials(Application app) {
        SharedPreferences prefs = app.getSharedPreferences("devRant", Context.MODE_PRIVATE);
        String rndGuid = makeGuid();
        String authTokenString;

        guid = prefs.getString(app.getString(R.string.preference_guid), rndGuid);
        authTokenString = prefs.getString(app.getString(R.string.preference_token), null);
        if (authTokenString != null) {
            authToken = new AuthToken(authTokenString);
            Log.e(TAG, authTokenString);
        } else {
            Log.d(TAG, "authTokenString was null, the user probably isnt logged in yet.");
        }
        username = prefs.getString(app.getString(R.string.preference_username), null);
    }

    /**
     * Clears the users credentials from the shared preferences.
     * @param app The app whose shared preferences to use.
     */
    public static void clearUserCredentials(Application app) {
        guid = null;
        authToken = null;

        SharedPreferences prefs = app.getSharedPreferences("devRant", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();

        broadcastLoginStateChanged();

        Log.d(TAG, "clearUserCredentials(): cleared everything.");
    }

    /**
     * Creates a new GUID make of 4 sets of 8, 4, 4 and 12 random hexadecimal chars respectively, separated by dashes.
     * @return The random new guid.
     */
    private static String makeGuid() {
        String newGuid;

        newGuid = makeRnd(8) + "-" + makeRnd(4) + "-" + makeRnd(4) + "-"
                + makeRnd(12);

        return newGuid;
    }

    /**
     * Generate a random hexadecimal string of a given length.
     * @param count The length of the string to generate.
     * @return The random string.
     */
    private static String makeRnd(int count) {
        String s = "";
        String avail = "0123456789abcdef";
        Random rnd = new Random();

        for (int i = 0; i < count; ++i) {
            s += avail.charAt((int) (rnd.nextFloat() * avail.length()));
        }

        return s;
    }

    /**
     * Tells all registered login state listener that the login state has changed.
     */
    public static void broadcastLoginStateChanged() {
        for (LoginStateListener listener : loginStateListeners) {
            Log.d(TAG, "broadcasting");
            listener.onLoginDataChanged();
        }
    }

    /**
     * Registers a login state listener.
     * @param listener The listener to register.
     */
    public static void registerLoginStateListener(LoginStateListener listener) {
        loginStateListeners.add(listener);
        Log.d(TAG, "registerLoginStateListener()");
    }

    /**
     * Deregisters a login state listener.
     * @param listener The listener to deregister.
     */
    public static void deregisterLoginStateListener(LoginStateListener listener) {
        loginStateListeners.remove(listener);
        Log.d(TAG, "deregisterLoginStateListener()");
    }
}
