package de.eigenanbauer.devrant.custom_elements;

import android.app.Activity;
import com.google.android.material.snackbar.Snackbar;
import android.view.View;

import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.activities.LoginActivity;
import de.eigenanbauer.devrant.data.Settings;

/**
 * A custom snackbar that asks the user to login and fixes androids gray bars on the right and left in black theme.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class LoginRequiredSnackbar {

    private Snackbar snackbar;
    private Activity activity;

    /**
     * Creates a custom snackbar fitting the current app theme.
     * Tells the snackbar to show a login button that enables the user to directly log in to devRant by opening a login activity.
     * @param view The parents view.
     * @param activity The parents activity.
     */
    public LoginRequiredSnackbar(View view, Activity activity) {
        this.activity = activity;
        this.snackbar = Snackbar.make(view, R.string.error_action_needs_login, Snackbar.LENGTH_SHORT).setAction(R.string.login, loginClickListener);

        // TODO: Ugly hack to make snackback background fully black in darktheme
        if (Settings.getUseDarkTheme(activity.getApplication()))
            this.snackbar.getView().setBackgroundColor(activity.getResources().getColor(R.color.black, activity.getTheme()));
    }

    /**
     * Shows the snackbar that was created and initialized in the constructor.
     */
    public void show() {
        snackbar.show();
    }

    /**
     * A OnClickListener for the login button that opens a LoginActivity.
     */
    private View.OnClickListener loginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginActivity.openLoginActivity(activity);
        }
    };
}
