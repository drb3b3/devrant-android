package de.eigenanbauer.devrant.custom_elements;

import android.content.Context;
import android.widget.Toast;

/**
 * Custom Toast to make the default toast more consistent and compatible with the black theme.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class DevRantToast {

    private Toast toast;

    /**
     * Constructor that creates a new custom toast.
     * @param context The activities context.
     * @param text The text to be displayed.
     * @param length The duration that the toast should be visible.
     */
    public DevRantToast(Context context, CharSequence text, int length) {
        toast = Toast.makeText(context, text, length);
        // TODO: fix stupid android toasts
        //toast.getView().setBackgroundColor(context.getResources().getColor(R.color.white, context.getTheme()));
    }

    /**
     * Shows the toast that was created and initialized when calling the constructor.
     */
    public void show() {
        toast.show();
    }
}
