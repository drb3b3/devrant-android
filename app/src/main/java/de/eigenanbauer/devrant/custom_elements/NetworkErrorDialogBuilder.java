package de.eigenanbauer.devrant.custom_elements;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import de.eigenanbauer.devrant.R;

/**
 * Defines a dialog that alerts the user about a network error.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class NetworkErrorDialogBuilder extends AlertDialog.Builder implements DialogInterface.OnClickListener {

    private static final String TAG = "NETWORK_ERROR_DIALOG_BUILDER";

    /**
     * Implements a custom DialogInterface. Sets the title and message to tell the user that there was a network error.
     * @param activity The activity that would like to open the dialog.
     */
    public NetworkErrorDialogBuilder(Activity activity) {
        super(activity);

        setTitle(R.string.error_network_error_title);
        setMessage(R.string.error_network_error);
        setPositiveButton(R.string.okay, this);
    }

    /**
     * Handles a click from the user on a button of the interface.
     * Doesn't do anything but logging the click.
     * @param dialog The dialog the that was clicked.
     * @param which The button the user clicked.
     */
    @Override
    public void onClick(DialogInterface dialog, int which) {
        Log.d(TAG, "onClick()");
    }
}
