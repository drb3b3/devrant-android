package de.eigenanbauer.devrant;

import java.util.ArrayList;

import de.eigenanbauer.devrant.data.Rant;

/**
 * An interface that receives new rants or handles a network error in case
 * something went wrong while loading new rants.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public interface RantCollector {

    /**
     * Gets called when new rants were loaded successfully.
     * @param newRants A list containing the new rants.
     */
    void addRants(ArrayList<Rant> newRants);

    /**
     * Gets called when there was a network error.
     */
    void onNetworkError();

}
