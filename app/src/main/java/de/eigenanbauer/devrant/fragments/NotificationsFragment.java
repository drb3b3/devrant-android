package de.eigenanbauer.devrant.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.custom_elements.DevRantToast;
import de.eigenanbauer.devrant.custom_elements.LoginRequiredSnackbar;
import de.eigenanbauer.devrant.data.LoginData;

/**
 * A fragment that displays all notifications of the user.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class NotificationsFragment extends Fragment {

    public static final String TAG = "NOTIFICATIONS_FRAGMENT";

    private TextView textViewTest;

    /**
     * Inflates the notification fragment. If the user is not signed in yet a snackbar kindly asks him to do so.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notifications, container, false);

        textViewTest = v.findViewById(R.id.textViewTest);

        if (!LoginData.hasUserCredentials()) {
            new LoginRequiredSnackbar(getView(), getActivity()).show();
            textViewTest.setText("Login pls");
        }

        return v;
    }
}
