package de.eigenanbauer.devrant.fragments;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;

import de.eigenanbauer.devrant.LoginStateListener;
import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.activities.LoginActivity;
import de.eigenanbauer.devrant.data.LoginData;

/**
 * This represents the settings fragment from which the user can setup his kinky preferences.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class SettingsFragment extends PreferenceFragmentCompat implements LoginStateListener {

    private static final String TAG = "SETTINGS_FRAGMENT";

    Preference preferenceLogin;
    SwitchPreferenceCompat switchPreferenceCompatDarkTheme;

    /**
     * Finds all the preferences.
     * @param savedInstanceState
     * @param rootKey
     */
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);

        preferenceLogin = findPreference(getActivity().getString(R.string.preference_key_login));
        switchPreferenceCompatDarkTheme = findPreference(getActivity().getString(R.string.preference_key_darktheme_switch));
        switchPreferenceCompatDarkTheme.getOnPreferenceChangeListener();

        update();
    }

    /**
     * Registers itself as a login state listener so it know when the user logged out or in.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginData.registerLoginStateListener(this);
    }

    /**
     * Deregisters itself from the list of login state listener so there wont be a segfault
     * when the user logs in but is not in the settings fragment at the moment.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        LoginData.deregisterLoginStateListener(this);
    }

    /**
     * Updates all preference views. The login preference is set depending on
     * whether the user is currently logged in or not. A on preference click listener
     * is set to the login preference that allows the user to sign in or sign out
     * depending on whether he is currently logged in or not.
     */
    public void update() {
        Log.d(TAG, "update()");

        /* update the login button */
        if (LoginData.hasUserCredentials()) {
            preferenceLogin.setTitle(R.string.preference_title_logout);
            preferenceLogin.setSummary("Logged in as " + LoginData.getUsername() + ".");
            preferenceLogin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Log.d(TAG, "Logout clicked.");
                    LoginData.clearUserCredentials(getActivity().getApplication());
                    Toast.makeText(getContext(), R.string.message_user_logout_successful, Toast.LENGTH_SHORT).show();
                    update();
                    return true;
                }
            });
        } else {
            preferenceLogin.setTitle(R.string.preference_title_login);
            preferenceLogin.setSummary(R.string.preference_summary_login);
            preferenceLogin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Log.d(TAG, "Login clicked");
                    LoginActivity.openLoginActivity(getActivity());
                    return true;
                }
            });
        }
    }

    /**
     * Updates all the preference view when the login data changed.
     */
    @Override
    public void onLoginDataChanged() {
        update();
    }
}