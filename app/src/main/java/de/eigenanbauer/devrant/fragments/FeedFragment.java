package de.eigenanbauer.devrant.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import de.eigenanbauer.devrant.custom_elements.LoginRequiredSnackbar;
import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.RantCollector;
import de.eigenanbauer.devrant.activities.RantPostActivity;
import de.eigenanbauer.devrant.adapters.FeedAdapter;
import de.eigenanbauer.devrant.async.FeedParser;
import de.eigenanbauer.devrant.custom_elements.NetworkErrorDialogBuilder;
import de.eigenanbauer.devrant.data.FeedType;
import de.eigenanbauer.devrant.data.LoginData;
import de.eigenanbauer.devrant.data.Rant;


/**
 * A fragment the devRant feed is displayed in.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class FeedFragment extends Fragment implements RantCollector, View.OnClickListener, View.OnLongClickListener {

    public static final String TAG = "FEED_FRAGMENT";

    private FloatingActionButton floatingActionButtonNewRant;
    private RecyclerView recyclerViewFeed;

    private FeedAdapter feedAdapter;
    private RecyclerView.LayoutManager feedLayoutManager;

    /**
     * Inflates the fragments view. If there was no previous feed adapter a new one is created.
     * The feeds recycler view is initialized and the "new rant" buttons onClick function is set.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_feed, container, false);

        Log.d(TAG, "onCreateView()");

        floatingActionButtonNewRant = v.findViewById(R.id.floatingActionButtonNewRant);
        recyclerViewFeed = v.findViewById(R.id.recyclerview_feed);

        if (feedAdapter == null) {
            feedAdapter = new FeedAdapter(this, recyclerViewFeed, new FeedParser(this));
            Log.d(TAG, "feed adapter was null");
        }


        recyclerViewFeed.setAdapter(feedAdapter);
        recyclerViewFeed.setItemViewCacheSize(64);
        feedAdapter.setRecyclerViewFeed(recyclerViewFeed);

        feedLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerViewFeed.setLayoutManager(feedLayoutManager);

        floatingActionButtonNewRant.setOnClickListener(this);

        return v;
    }

    /**
     * If there is a feed adapter its feed parser is set to a new feed parser parsing the given feed type.
     * @param feedType The new feed type.
     */
    public void setFeedType(FeedType feedType) {
        if (feedAdapter != null)
            feedAdapter.setFeedParser(new FeedParser(this, feedType));
    }

    /**
     * Reloads the feed.
     */
    public void reloadFeed() {
        feedAdapter.setFeedParser(new FeedParser(this, feedAdapter.getFeedType()));
        Log.d(TAG, "reloadFeed()");
    }

    /**
     * Add rants to the feed adapter.
     * @param newRants A list containing the new rants.
     */
    @Override
    public void addRants(final ArrayList<Rant> newRants) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    feedAdapter.addRants(newRants);
                }
            });
        }
    }

    /**
     * Handles network failures while loading new rants.
     */
    @Override
    public void onNetworkError() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new NetworkErrorDialogBuilder(getActivity()).create().show();
            }
        });
    }

    /**
     * Opens a new alert dialog where the user can select his feed type from.
     * @param v The view that was long clicked.
     * @return Always returns true.
     */
    @Override
    public boolean onLongClick(View v) {
        String[] feedTypes = { "Algo", "Recent", "Top All", "Top Month", "Top Week", "Top Day" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.feed_type_popup_title);
        builder.setItems(feedTypes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FeedType feedType;
                switch (which) {
                    case 0:
                        feedType = FeedType.FEEDTYPE_ALGO;
                        break;
                    case 1:
                        feedType = FeedType.FEEDTYPE_RECENT;
                        break;
                    case 2:
                        feedType = FeedType.FEEDTYPE_TOP_ALL;
                        break;
                    case 3:
                        feedType = FeedType.FEEDTYPE_TOP_MONTH;
                        break;
                    case 4:
                        feedType = FeedType.FEEDTYPE_TOP_WEEK;
                        break;
                    case 5:
                        feedType = FeedType.FEEDTYPE_TOP_DAY;
                        break;
                    default:
                        Toast.makeText(getContext(), "Wtf feed type did you select?", Toast.LENGTH_SHORT).show();
                        return;
                }

                feedAdapter.setFeedParser(new FeedParser(FeedFragment.this, feedType));
            }
        });
        builder.show();

        return true;
    }

    /**
     * Shows the "create new rant" button in case it is currently invisible.
     */
    public void showCreateNewRantButton() {
        if (floatingActionButtonNewRant.getVisibility() != View.VISIBLE)
            floatingActionButtonNewRant.show();
    }

    /**
     * Hides the "create new rant" button in case it is currently visible.
     */
    public void hideCreateNewRantButton() {
        if (floatingActionButtonNewRant.getVisibility() == View.VISIBLE)
            floatingActionButtonNewRant.hide();
    }

    /**
     * If the user is logged in an activity is opened where the user can rant about his painful life.
     * If the user is not logged in a Snackbar kindly asks him to sign in.
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (LoginData.hasUserCredentials()) {
            RantPostActivity.openCreateRantActivity(FeedFragment.this.getActivity());
        } else {
            new LoginRequiredSnackbar(v, getActivity()).show();
        }
    }
}
