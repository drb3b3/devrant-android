package de.eigenanbauer.devrant;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.eigenanbauer.devrant.adapters.FeedAdapter;

/**
 * A onScrollListener that detects when the user reaches the bottom given a fixed
 * threshold and scrolls up or down.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class FeedAdapterOnScrollListener extends RecyclerView.OnScrollListener {

    private FeedAdapter feedAdapter;

    public FeedAdapterOnScrollListener(FeedAdapter feedAdapter) {
        this.feedAdapter = feedAdapter;
    }

    /**
     * Starts loading new rants if the user scrolled to the bottom with a given threshold.
     * If the user scrolls downwards the feed fragments create new rant button gets hidden,
     * if the user scrolls upwards the create new rant button is set to be visible again.
     * @param recyclerView Our recycler view.
     * @param dx The horizontal scoll amount.
     * @param dy The vertical scroll amount.
     */
    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        final int threshold = 20;
        int totalItems = feedAdapter.getItemCount();
        int lastItemVisible = layoutManager.findLastVisibleItemPosition();

        if (lastItemVisible + threshold >= totalItems - 1)
            feedAdapter.bottomThresholdPassed();

        if (dy < 0) {
            feedAdapter.onScrolledUp();
        } else if (dy > 0) {
            feedAdapter.onScrolledDown();
        }
    }
}
