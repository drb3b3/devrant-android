package de.eigenanbauer.devrant.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import de.eigenanbauer.devrant.R;

/**
 * Activity for viewing a rant and it's comments.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class RantViewerActivity extends DevRantActivity {

    private static final String TAG = "RANT_VIEWER_ACTIVITY";
    private static final String RANT_URL_EXTRA = "ranturl";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rant_viewer);

        Intent intent = getIntent();

        String rantUrl = intent.getStringExtra(RANT_URL_EXTRA);
        if (rantUrl == null)
            Log.e(TAG, "Rant url from intent was null.");

        Toast.makeText(getApplicationContext(), "Rant: " + rantUrl, Toast.LENGTH_LONG).show();
    }

    public static void showRant(Activity parentActivity, String url) {
        Intent intent = new Intent(parentActivity, RantViewerActivity.class);
        intent.putExtra(RANT_URL_EXTRA, url);
        parentActivity.startActivity(intent);
    }
}
