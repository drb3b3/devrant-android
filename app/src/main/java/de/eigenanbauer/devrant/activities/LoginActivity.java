package de.eigenanbauer.devrant.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.data.LoginData;
import de.eigenanbauer.devrant.async.LoginParserAsyncTask;

/**
 * Shows input fields so that the user can enter his username and password.
 * By clicking on the login button the user is either logged in and a authentication
 * token is saved inside the shared preferences or the user is given a proper error message.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class LoginActivity extends DevRantActivity implements View.OnClickListener {

    private static final String TAG = "LOGIN_ACTIVITY";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private Button buttonLogin;

    /**
     * Gets called when the activity is created. Initializes all views of the activity.
     * Sets a onClickListener to the login button.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsername = findViewById(R.id.editTextLoginUsername);
        editTextPassword = findViewById(R.id.editTextLoginPassword);
        buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(this);
    }

    /**
     * Starts a new login activity.
     * @param parent The activity that would like to open the login activity.
     */
    public static void openLoginActivity(Activity parent) {
        Intent intent = new Intent(parent, LoginActivity.class);
        parent.startActivity(intent);
    }

    /**
     * Handles the user clicking on the login button.
     * If the user didn't enter a username or password a proper error message is displayed.
     * If the user entered his credentials the program tries to log the user in.
     * @param view
     */
    @Override
    public void onClick(View view) {
        if (editTextUsername.getText() == null || editTextUsername.getText().length() == 0) {
            editTextUsername.setError("Please enter your username.");
        } else if (editTextPassword.getText() == null || editTextPassword.getText().length() == 0) {
            editTextPassword.setError("Please enter your password.");
        }

        new LoginParserAsyncTask().login(this, editTextUsername.getText().toString(), editTextPassword.getText().toString());
    }

    /**
     * Gets called when the user entered incorrect credentials or another error occured during the login process.
     * Toasts an error message to the user and marks the password field with an error message.
     * @param json The error message sent by the devRant API or another error.
     */
    public void onFailedLogin(final String json) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), R.string.error_login_failed, Toast.LENGTH_SHORT).show();
                editTextPassword.setError("Please check your password.");
            }
        });
    }

    /**
     * Gets called when the login was successful.
     * Parses the authentication token returned by the devRant API and stores it.
     * @param username The username the user logged in with.
     * @param json The json response from the devRant API.
     */
    public void onSuccessfulLogin(final String username, final String json) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject jobj = new JSONObject(json);
                    Toast.makeText(getApplicationContext(), "Login succeeded", Toast.LENGTH_SHORT).show();
                    LoginData.setCredentials(jobj.getJSONObject("auth_token").toString());
                    LoginData.setUsername(username);
                    finish();
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
    }
}
