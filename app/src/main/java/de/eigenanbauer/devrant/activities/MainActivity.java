package de.eigenanbauer.devrant.activities;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.MenuItem;

import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.data.FeedType;
import de.eigenanbauer.devrant.fragments.FeedFragment;
import de.eigenanbauer.devrant.fragments.NotificationsFragment;
import de.eigenanbauer.devrant.fragments.SettingsFragment;

/**
 * Shows the main screen of the devRant app. The user can navigate through
 * the various pages of the application like the his feed, the settings tab
 * or his notifications.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class MainActivity extends DevRantActivity {

    private static final String TAG = "MAIN_ACTIVITY";

    private BottomNavigationView navView;
    private int currentBottomNav = 0;

    private SettingsFragment settingsFragment;
    private FeedFragment feedFragment;
    private NotificationsFragment notificationsFragment;

    /**
     * Gets called when the activity is created.
     * Initializes all views.
     * Sets the listener of the bottom navigation bar.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate()");
        Log.d(TAG, "pimml");

        settingsFragment = new SettingsFragment();
        feedFragment = new FeedFragment();
        notificationsFragment = new NotificationsFragment();

        /* bottom navigation view */
        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.setSelectedItemId(R.id.navigation_feed);
    }

    /**
     * Gets called when a rant was posted.
     * If the rant was posted successfully the feed is reloaded and its feed type is set to recent so the user can see his rant.
     * @param requestCode
     * @param resultCode States whether the rant was posted successfully or not.
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            feedFragment.setFeedType(FeedType.FEEDTYPE_RECENT);
            Log.d(TAG, "Rant posted successfully");
        } else {
            Log.d(TAG, "Rant post unsuccessful");
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment;
            switch (item.getItemId()) {
                case R.id.navigation_settings:
                    selectedFragment = settingsFragment;
                    break;
                case R.id.navigation_feed:
                    selectedFragment = feedFragment;
                    /* make the feed reload in case the user tabs the feed button if he's already viewing the feed */
                    if (currentBottomNav == item.getItemId())
                        feedFragment.setFeedType(FeedType.FEEDTYPE_RECENT);
                    break;
                case R.id.navigation_notifications:
                    selectedFragment = notificationsFragment;
                    break;
                default:
                    return false;
            }

            currentBottomNav = item.getItemId();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main, selectedFragment, selectedFragment.getTag()).commit();

            return true;
        }
    };
}
