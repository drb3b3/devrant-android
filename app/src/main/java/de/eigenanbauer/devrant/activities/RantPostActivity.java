package de.eigenanbauer.devrant.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.RantPostSuccessListener;
import de.eigenanbauer.devrant.async.RantPosterAsyncTask;

/**
 * Displays various input fields so the user can enter his rant's text, tags, select a rant type.
 * The rant is posted by a click on the rant button. If ranting failed the user is given an error message.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class RantPostActivity extends DevRantActivity implements View.OnClickListener, RantPostSuccessListener {

    private static final String TAG = "CREATE_RANT_ACTIVITY";

    private EditText editTextRantText;
    private EditText editTextRantTags;
    private Spinner spinnerRantType;
    private Button buttonSubmitRant;

    /**
     * Sets the content view to activity_create_rant,
     * initializes all views. Sets a on click listener to the submit button and fills
     * the spinner with possible rant types.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_rant);

        editTextRantText = findViewById(R.id.editTextRantText);
        editTextRantTags = findViewById(R.id.editTextRantTags);
        spinnerRantType = findViewById(R.id.spinnerRantType);
        buttonSubmitRant = findViewById(R.id.buttonSubmitRant);

        editTextRantText.addTextChangedListener(textChangedListener);

        buttonSubmitRant.setOnClickListener(this);

        ArrayAdapter<CharSequence> rantTypeAdapter = ArrayAdapter.createFromResource(this, R.array.rant_types, android.R.layout.simple_spinner_dropdown_item);
        spinnerRantType.setAdapter(rantTypeAdapter);
    }

    TextWatcher textChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Opens a new CreateRantActivity.
     * @param parentActivity The parent activity to which the CreateRantActivity will return.
     */
    public static void openCreateRantActivity(Activity parentActivity) {
        Intent intent = new Intent(parentActivity, RantPostActivity.class);
        parentActivity.startActivityForResult(intent, 31);
    }

    /**
     * Collects the data of all views in the activity and creates a new post with that data.
     * @param view The view that was clicked.
     */
    @Override
    public void onClick(View view) {
        Log.d(TAG, "posting rant.");
        String tags = editTextRantTags.getText().toString();
        if (tags.length() == 0)
            tags = tagFromRantType(spinnerRantType.getSelectedItem().toString());
        else
            tags += ", " + tagFromRantType(spinnerRantType.getSelectedItem().toString());


        new RantPosterAsyncTask(this, editTextRantText.getText().toString(), editTextRantTags.getText().toString());
    }

    /**
     * Gets called if posting the rant failed. Makes a toast to inform the user about the failure.
     * @param json A json string containing the error message returned from the devRant API.
     */
    @Override
    public void onRantPostFailure(final String json) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "rantPostFailure: " + json);
                Toast.makeText(getApplicationContext(), R.string.error_rant_post_failed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Gets called when a rant was posted successfully. Makes a toast to inform the user that his rant was posted.
     * Returns to the parent activity.
     * @param json The message returned by the devRant API.
     */
    @Override
    public void onRantPostSuccess(final String json) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "rantPostSuccess: " + json);
                Toast.makeText(getApplicationContext(), R.string.message_rant_post_successful, Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    /**
     * Generates the tag string needed by the devRant API for posting a rant.
     * @param rantType The rant type as a string as represented in the rant type spinner.
     * @return The rant type in a format that the devRant API understands. If rantType is not a valid rant type the default value of "random" is chosen.
     */
    private String tagFromRantType(String rantType) {
        if (rantType.equalsIgnoreCase("Rant/Story"))
            return "rant";
        else if (rantType.equalsIgnoreCase("Joke/Meme"))
            return "joke/meme";
        else if (rantType.equalsIgnoreCase("Question"))
            return "question";
        else if (rantType.equalsIgnoreCase("devRant"))
            return "devrant";
        else if (rantType.equalsIgnoreCase("Random"))
            return "random";

        Log.e(TAG, "tagFromRantType(): Unknown ranttype: " + rantType);
        return "random";
    }
}
