package de.eigenanbauer.devrant.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import de.eigenanbauer.devrant.R;

/**
 * Shows a image passed to the activity in the intent.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class ImageViewerActivity extends DevRantActivity {

    private static final String TAG = "IMAGE_VIEWER_ACTIVITY";
    private static final String IMAGE_URL_EXTRA = "imageurl";

    private ImageView imageViewImageViewer;

    /**
     * Initializes all views. Additionally it reads the requested image URL to display from the opening intent.
     * If there was no URL passed an Toast displays an error message to the user and the activity is closed.
     * If there was a URL passed it is downloaded and rendered into imageViewImageViewer.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);

        imageViewImageViewer = findViewById(R.id.imageViewImageViewer);

        Intent intent = getIntent();
        String url = intent.getStringExtra(IMAGE_URL_EXTRA);
        if (url == null) {
            Toast.makeText(getApplicationContext(), "Are you seriously opening the image viewer without any image URL? Stupid cunt.", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Activity opened without imageurl extra put into intent.");
            finish();
        }

        Toast.makeText(getApplicationContext(), "TODO: properly implement image viewer activity", Toast.LENGTH_SHORT).show();
        Picasso.get().load(url).into(imageViewImageViewer);
    }

    /**
     * Opens a new ImageViewerActivity and passes the image URL to it.
     * @param parentActivity The activity that would like to open the image.
     * @param url The URL of the image to display.
     */
    public static void showImageFromURL(Activity parentActivity, String url) {
        Intent intent = new Intent(parentActivity, ImageViewerActivity.class);
        intent.putExtra(IMAGE_URL_EXTRA, url);
        parentActivity.startActivity(intent);
    }
}
