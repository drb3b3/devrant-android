package de.eigenanbauer.devrant.activities;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.data.LoginData;
import de.eigenanbauer.devrant.data.Settings;

/**
 * Represents a base class for all activities of the app. This allows easy
 * implementation of global saves inside shared preferences. Deriving classes
 * will not have to worry about loading saved data from preferences.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public abstract class DevRantActivity extends AppCompatActivity {

    private static final String TAG = "DEV_RANT_ACTIVITY";

    /**
     * Gets called when the activity is created.
     * Loads the users credentials so the deriving Activity does not have to worry about doing so.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LoginData.loadSavedCredentials(getApplication());
        updateTheme(); // Has to be called before super.onCreate() and setContentView()!

        super.onCreate(savedInstanceState);
    }

    /**
     * Gets called when the activity is destroyed. For example when the user
     * exits the app or the app is cleared in the background to free some memory.
     * Loads the users credentials so the deriving Activity does not have to worry about doing so.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        LoginData.saveCredentials(getApplication());
    }

    /**
     * Gets called when a activity is resumed. For example when the user switches from another activity to this one.
     * Loads the users credentials so the deriving Activity does not have to worry about doing so.
     * Also sets the app theme to dark theme in case the user activated that in the settings.
     */
    @Override
    protected void onResume() {
        super.onResume();
        LoginData.loadSavedCredentials(getApplication());
    }

    /**
     * Gets called when an activity is paused. For example when the user switches to another app or before the activity is destroyed.
     * Saves the users credentials so the deriving Activity does not have to worry about doing so.
     */
    @Override
    protected void onPause() {
        super.onPause();
        LoginData.saveCredentials(getApplication());
    }

    /**
     * Gets called when a activity is started. For example when the user switches from another activity to this one.
     * Loads the users credentials so the deriving Activity does not have to worry about doing so.
     */
    @Override
    protected void onStart() {
        super.onStart();
        LoginData.loadSavedCredentials(getApplication());
    }

    /**
     * Gets called when an activity is stopped. For example when the user switches to another activity and this one becomes invisible.
     * Saves the users credentials so the deriving Activity does not have to worry about doing so.
     */
    @Override
    protected void onStop() {
        super.onStop();
        LoginData.saveCredentials(getApplication());
    }

    /**
     * Sets the activities theme based on the one selected in the Settings Menu.
     *
     */
    protected void updateTheme() {
        Log.d(TAG, "updateTheme");

        if (Settings.getUseDarkTheme(getApplication())) {
            Log.d(TAG, "using dark theme");
            setTheme(R.style.AppThemeDark);
            // TODO: there has to be a better way to do it.
            getWindow().setNavigationBarColor(getResources().getColor(R.color.black, getTheme()));
        } else {
            Log.d(TAG, "using ugly theme");
            getApplication().setTheme(R.style.AppTheme);
        }
    }
}
