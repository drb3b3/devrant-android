package de.eigenanbauer.devrant;

/**
 * Represents an object that reacts to changes of the global login state.
 * A class implementing this interface can be registered in the LoginData
 * class.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public interface LoginStateListener {

    /**
     * Gets called as soon as the login data changes.
     * This happens when the user either logs out or in.
     */
    void onLoginDataChanged();

}
