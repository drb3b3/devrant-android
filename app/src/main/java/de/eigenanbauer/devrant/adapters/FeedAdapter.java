package de.eigenanbauer.devrant.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.eigenanbauer.devrant.FeedAdapterOnScrollListener;
import de.eigenanbauer.devrant.RantCollector;
import de.eigenanbauer.devrant.RantVoteSuccessListener;
import de.eigenanbauer.devrant.custom_elements.LoginRequiredSnackbar;
import de.eigenanbauer.devrant.R;
import de.eigenanbauer.devrant.activities.ImageViewerActivity;
import de.eigenanbauer.devrant.activities.RantViewerActivity;
import de.eigenanbauer.devrant.async.FeedRantVoter;
import de.eigenanbauer.devrant.custom_elements.NetworkErrorDialogBuilder;
import de.eigenanbauer.devrant.data.FeedType;
import de.eigenanbauer.devrant.data.LoginData;
import de.eigenanbauer.devrant.data.Rant;
import de.eigenanbauer.devrant.async.FeedParser;
import de.eigenanbauer.devrant.data.VoteState;
import de.eigenanbauer.devrant.fragments.FeedFragment;

/**
 * An adapter that handles the items of the feed fragment. If the user is near the bottom
 * of the feed more feed elements are loaded automatically. The user can interact with
 * the rants by either liking or disliking it, opening the rant by clicking on it,
 * viewing the rants image in case is has one.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedRantViewHolder> implements RantCollector {

    private static final String TAG = "FEED_ADAPTER";

    private FeedFragment feedFragment;
    private FeedParser parser;
    private ArrayList<Rant> rants;
    private RecyclerView recyclerViewFeed;

    /**
     * Defines the view that contains the rant, it's elements like vote count, the image and comment count.
     * Also shows buttons to vote the rant.
     */
    public class FeedRantViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, RantVoteSuccessListener {

        private Button buttonFeedUpvoteRant;
        private Button buttonFeedDownvoteRant;
        private TextView textViewFeedRantVoteCount;
        private TextView textViewRantText;
        private ImageView imageViewFeedRantImage;
        private TextView textViewFeedRantTags;
        private TextView textViewFeedRantCommentCount;

        private Rant rant;

        /**
         * Initializes all views. sets an onclick listener to the up- and downvote buttons
         * that will call the devRant API to like or dislike this rant.
         * Sets an onClickListener to itself that will open the rant.
         * Also sets an onLongClick listener to itself that will open a window that asks
         * the user for the type of feed he wants to view.
         * @param viewRant The view that is going to hold the rant.
         */
        public FeedRantViewHolder(View viewRant) {
            super(viewRant);
            buttonFeedUpvoteRant = viewRant.findViewById(R.id.buttonFeedUpvoteRant);
            buttonFeedDownvoteRant = viewRant.findViewById(R.id.buttonFeedDownvoteRant);
            textViewFeedRantVoteCount = viewRant.findViewById(R.id.textViewFeedRantVoteCount);
            textViewRantText = viewRant.findViewById(R.id.textViewFeedRantText);
            imageViewFeedRantImage = viewRant.findViewById(R.id.imageViewFeedRantImage);
            textViewFeedRantTags = viewRant.findViewById(R.id.textViewFeedRantTags);
            textViewFeedRantCommentCount = viewRant.findViewById(R.id.textViewFeedRantCommentCount);

            buttonFeedUpvoteRant.setOnClickListener(new View.OnClickListener() {
                /**
                 * Shows a Snackbar that asks the user to sign in in case he is not signed in at the moment.
                 * If the user is signed in it will call the devRant API to either upvote the rant in case is
                 * is not upvoted yet. If it is upvoted the upvote will be removed.
                 * @param upvoteButton The view that contains the upvote button.
                 */
                @Override
                public void onClick(View upvoteButton) {
                    if (LoginData.hasUserCredentials()) {
                        Log.d(TAG, LoginData.getAuthTokenAsString());
                        if (rant.getVoteState() == VoteState.UPVOTED) {
                            new FeedRantVoter(FeedRantViewHolder.this, VoteState.UNVOTED, rant.getDataId()).execute();
                        } else {
                            new FeedRantVoter(FeedRantViewHolder.this, VoteState.UPVOTED, rant.getDataId()).execute();
                        }
                    } else {
                        new LoginRequiredSnackbar(recyclerViewFeed, feedFragment.getActivity()).show();
                    }
                }
            });
            buttonFeedDownvoteRant.setOnClickListener(new View.OnClickListener() {
                /**
                 * Shows a Snackbar that asks the user to sign in in case he is not signed in at the moment.
                 * If the user is signed in it will call the devRant API to either downvote the rant in case is
                 * is not downvoted yet. If it is downvoted the downvote will be removed.
                 * @param downvoteButton The view that contains the downvote button.
                 */
                @Override
                public void onClick(View downvoteButton) {
                    if (LoginData.hasUserCredentials()) {
                        if (rant.getVoteState() == VoteState.DOWNVOTED) {
                            new FeedRantVoter(FeedRantViewHolder.this, VoteState.UNVOTED, rant.getDataId()).execute();
                        } else {
                            new FeedRantVoter(FeedRantViewHolder.this, VoteState.DOWNVOTED, rant.getDataId()).execute();
                        }
                    } else {
                        new LoginRequiredSnackbar(recyclerViewFeed, feedFragment.getActivity()).show();
                    }
                }
            });

            viewRant.setOnClickListener(this);
            viewRant.setOnLongClickListener(feedFragment);

            imageViewFeedRantImage.setOnClickListener(new View.OnClickListener() {
                /**
                 * Opens a new ImageViewActivity that shows the currently displayed rants image.
                 * @param imageView The view of the rants image.
                 */
                @Override
                public void onClick(View imageView) {
                    ImageViewerActivity.showImageFromURL(feedFragment.getActivity(), rant.getImageURL());
                }
            });
        }

        /**
         * Sets the rant to be displayed in the view holder.
         * Sets the text of various views of this view holder.
         * If the rant has an image it will be downloaded and displayed.
         * @param rant The rant to be displayed.
         */
        public void setRant(final Rant rant) {
            this.rant = rant;

            textViewRantText.setText(rant.getText());
            Picasso.get().load(rant.getImageURL()).into(imageViewFeedRantImage);
            textViewFeedRantTags.setText(rant.createTagsString());
            textViewFeedRantCommentCount.setText(rant.getCommentCount() + " comments");

            updateVotes();
        }

        /**
         * @return The rant that is currently bound the the view holder.
         */
        public Rant getRant() {
            return rant;
        }

        /**
         * Opens new rant view activity that shows the rant that is currenty bound to this view holder.
         * @param v Not used.
         */
        @Override
        public void onClick(View v) {
            RantViewerActivity.showRant(feedFragment.getActivity(), rant.getRantURL());
        }

        /**
         * Updates the vote views of the rant by setting the current vote count and sets the background images
         * of the upvote and downvote buttons depending on whether the user upvoted or downvoted.
         */
        private void updateVotes() {
            feedFragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textViewFeedRantVoteCount.setText(new Integer(rant.getVoteCount()).toString());

                    switch (rant.getVoteState()) {
                        case UPVOTED:
                            buttonFeedUpvoteRant.setBackground(feedFragment.getActivity().getDrawable(R.drawable.upvoted));
                            buttonFeedDownvoteRant.setBackground(feedFragment.getActivity().getDrawable(R.drawable.downvote));
                            break;
                        case DOWNVOTED:
                            buttonFeedUpvoteRant.setBackground(feedFragment.getActivity().getDrawable(R.drawable.upvote));
                            buttonFeedDownvoteRant.setBackground(feedFragment.getActivity().getDrawable(R.drawable.downvoted));
                            break;
                        case UNVOTED:
                            buttonFeedUpvoteRant.setBackground(feedFragment.getActivity().getDrawable(R.drawable.upvote));
                            buttonFeedDownvoteRant.setBackground(feedFragment.getActivity().getDrawable(R.drawable.downvote));
                            break;
                        default:
                            break;
                    }
                }
            });
        }

        /**
         * Updates the rants votestate, it's total votes and shows a Toast to inform the user
         * about the successful vote.
         * @param msg A nice message for the user.
         * @param newVoteState The new vote state of the rant.
         */
        @Override
        public void onVoteSuccess(final String msg, VoteState newVoteState, final int newVoteCount) {
            rant.setVoteState(newVoteState);
            rant.setVoteCount(newVoteCount);

            updateVotes();
            feedFragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(feedFragment.getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * Informs the user about a failed vote call to the devRant API by toasting a error message.
         * @param msg A neat message for the apps user.
         */
        @Override
        public void onVoteFailure(final String msg) {
            feedFragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(feedFragment.getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Creates a new FeedAdapter, sets it's reference to the recycler view and starts loading rants from the feed.
     * @param feedFragment The fragment that contains the feed.
     * @param recyclerView The recycler view to display the rants in.
     * @param parser A parser to use for loading more rants.
     */
    public FeedAdapter(FeedFragment feedFragment, RecyclerView recyclerView, FeedParser parser) {
        this.feedFragment = feedFragment;
        this.parser = parser;
        this.rants = new ArrayList<>();

        setRecyclerViewFeed(recyclerView);
        parser.loadIfNotAlreadyLoading();
    }

    /**
     * Sets the adapters feed parser. If there was a previous parser it gets disabled so that it wont send any new rants to us in the future.
     * @param newParser The new feed parser
     */
    public void setFeedParser(FeedParser newParser) {
        /* so the parser wont send us old rants that are not of our type anymore */
        if (parser != null)
            parser.disable();

        /* set our new parser */
        this.parser = newParser;
        this.rants = new ArrayList<>();
        notifyDataSetChanged();
        this.parser.loadIfNotAlreadyLoading();
        Log.d(TAG, "setFeedParser()");
    }

    /**
     * @return The current feed type.
     */
    public FeedType getFeedType() {
        return parser.getFeedType();
    }

    /**
     * Sets the recycler view of this adapter and registers a custom onScrollListener.
     * @param recyclerViewFeed The new recycler view.
     */
    public void setRecyclerViewFeed(RecyclerView recyclerViewFeed) {
        this.recyclerViewFeed = recyclerViewFeed;
        recyclerViewFeed.addOnScrollListener(new FeedAdapterOnScrollListener(this));
        notifyDataSetChanged();
        Log.d(TAG, "setRecyclerViewFeed()");
    }

    /**
     * Adds new rants and updates the recycler view accordingly.
     * @param newRants The new rants to add.
     */
    @Override
    public void addRants(ArrayList<Rant> newRants) {
        rants.addAll(newRants);
        notifyItemRangeInserted(rants.size() - newRants.size(), newRants.size());

        Log.d(TAG, "Rant Feed now has " + rants.size() + " rants.");
    }

    /**
     * Notifies the user about a network error while parsing new rants.
     */
    @Override
    public void onNetworkError() {
        feedFragment.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new NetworkErrorDialogBuilder(feedFragment.getActivity()).create().show();
            }
        });
    }

    /**
     * Creates a new viewholder for a rant based on our own layout called "feed_rantview".
     * @param parent The parents view group.
     * @param viewType The view type.
     * @return The new FeedRantViewHolder that was created and inflated.
     */
    @NonNull
    @Override
    public FeedRantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_rantview, parent, false);
        FeedRantViewHolder vh = new FeedRantViewHolder(v);
        return vh;
    }

    /**
     * Sets the specific rant to a view holder when it becomes visible.
     * @param feedRantViewHolder The view holder that needs data.
     * @param position The position of the rant in the rant list.
     */
    @Override
    public void onBindViewHolder(@NonNull FeedRantViewHolder feedRantViewHolder, int position) {
        feedRantViewHolder.setRant(rants.get(position));
    }

    /**
     * @return The amount of rants the adapter currently contains.
     */
    @Override
    public int getItemCount() {
        return rants.size();
    }

    /**
     * Called by our custom FeedAdapterOnScrollListener to notify us
     * about the user having scrolled to the bottom with a given threshold.
     * Tells the feed parser to load new rants.
     */
    public void bottomThresholdPassed() {
        parser.loadIfNotAlreadyLoading();
    }

    /**
     * Called by our custom FeedAdapterOnScrollListener to notify us
     * about the user having scrolled down.
     * Tells the feed fragment to hide the floating new rant button.
     */
    public void onScrolledDown() {
        feedFragment.hideCreateNewRantButton();
    }

    /**
     * Called by our custom FeedAdapterOnScrollListener to notify us
     * about the user having scrolled up.
     * Tells the feed fragment to show the floating new rant button.
     */
    public void onScrolledUp() {
        feedFragment.showCreateNewRantButton();
    }
}
