package de.eigenanbauer.devrant;

/**
 * An interface handling success or failure when posting a new rant.
 *
 * @author b3b3 <b3b3@koernel.org>
 */
public interface RantPostSuccessListener {

    /**
     * Called when the new rant was posted successfully.
     * @param json The json answer from the devRant API.
     */
    void onRantPostSuccess(String json);

    /**
     * Called when posting the new rant failed.
     * @param json The json answer from the devRant API.
     */
    void onRantPostFailure(String json);

}
